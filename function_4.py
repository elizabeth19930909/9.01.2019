# coding=utf-8
def square(length, width=1):
    return length*width

print square(2) # Вызов функции с одним аргументом. Так как width=1 по умолчанию, то значение функции = 2*1=2
print square(2, 4) # Вызов функции с 2мя аргументами. Так как теперь width=4, то значение функции = 2*4=8