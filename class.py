from math import sqrt
class Coordinates():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def distance(self):

        return 'Distance = %s' % (sqrt(self.x**2+self.y**2))

obj1 = Coordinates (x = 3,y = 4)

print 'Coordinates of the first object: x = %s, y = %s' % (obj1.x, obj1.y)
print obj1.distance()

obj2 = Coordinates (x = 9,y = 2)

print 'Coordinates of the second object: x = %s, y = %s' % (obj2.x, obj2.y)
print obj2.distance()