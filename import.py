# coding=utf-8
# импортируется все содержание файла
import mymodule
print mymodule.func(2,3)

# импортируется только функция
# from mymodule import func
# print func(2,3)

# импорт функции, которой присваивается другое имя
from mymodule import func as G
print G(2,3)