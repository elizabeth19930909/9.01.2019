def func (numbers):
    for i in numbers:
        if i%2 == 0:
            yield i

x = func (range(1, 10))
print next(x)
print next(x)
print next(x)


